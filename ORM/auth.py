# Пакет авторизации
from fastapi import APIRouter, Depends, status, HTTPException, Response
from sqlalchemy.orm import Session

# Добавление нужных модулей нашей API
from . import database, schemas, models, utils, oauth2

# Формочка для запроса пароля
from fastapi.security.oauth2 import OAuth2PasswordRequestForm


# Создание роутера
# Роутеры позволяют прописать функции и Url-пути в отдельном файле и всё разом
# подключить в точку входа и основного приложения, т.е. в CRUD_ORM.py. Т.е. это дополнительные энтрипоинты все к той же API
router = APIRouter(tags=['Authentication'])


# Можно авторизоваться с помощью своей формы
# В этом случае в postman следует передавать данные в Body->raw->JSON
@router.post('/login')
def login(author_credentials: schemas.AuthorLogin, db: Session = Depends(database.get_db)):
    # Получение пользователя с бд
    author = db.query(models.Author).filter(models.Author.email == author_credentials.email).first()

    # Проверка наличия пользователя (то что он вернулся из БД)
    if not author:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Invalid Credentials"
        )

    # Проверка верификации, если у пользователя хэш паролей не совпадает -> возвращаем ошибку
    if not utils.verify(author_credentials.password, author.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Invalid Credentials"
        )

    # Создаём токен из пользователя что залогинился
    access_token = oauth2.create_access_token(data={"author_id": author.id})

    return {"access_token": access_token, "token_type": "bearer"}

    # После получения токена следует его скопировать и использовать в postman

    # Проверить правильно JWT токена можно посмотреть по https://jwt.io/ во вкладке Debugger


# Можно авторизоваться с помощью формы OAuth2PasswordRequestForm (email заменяется username)
# В этом случае в postman следует передавать данные в Body->form-data
@router.post('/login/oauth')
def login_oauth(author_credentials: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(database.get_db)):
    author = db.query(models.Author).filter(models.Author.email == author_credentials.username).first()

    if not author:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Invalid Credentials"
        )

    if not utils.verify(author_credentials.password, author.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Invalid Credentials"
        )

    access_token = oauth2.create_access_token(data={"author_id": author.id})

    return {"access_token": access_token, "token_type": "bearer"}