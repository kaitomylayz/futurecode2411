import uvicorn
from fastapi import FastAPI, Response, status, HTTPException, Depends
from sqlalchemy.orm import Session

from .database import engine, get_db
from . import models, schemas, utils, auth, oauth2

# Шифрование пароля
from passlib.context import CryptContext
# Создание механизма шифрования
# Также можно перенести любые отдельные куски кода (особенно повторяющиеся) в utils.py
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

models.Base.metadata.create_all(bind=engine)

app = FastAPI(debug=True)

# Дополнительный роутер взятый из auth
app.include_router(auth.router)

@app.get("/posts")
async def all_posts(db: Session = Depends(get_db)):
    posts = db.query(models.Post).all()
    return {"data": posts}


@app.get("/posts/valid", response_model=list[schemas.Post])
async def all_posts_valid(db: Session = Depends(get_db)):
    posts = db.query(models.Post).all()
    return posts


# @app.get("/posts/{id}")
# async def one_post(id: int, db: Session = Depends(get_db)):
#     post = db.query(models.Post).filter(models.Post.id == id).first()
#     if post == None:
#         raise HTTPException (
#             status_code=status.HTTP_404_NOT_FOUND,
#             detail=f"post with id: {id} does not exist"
#         )
#     return {"data": post}


@app.get("/posts/valid/{id}", response_model=schemas.Post)
async def one_post_valid(id: int, db: Session = Depends(get_db)):
    post = db.query(models.Post).filter(models.Post.id == id).first()
    if post == None:
        raise HTTPException (
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"post with id: {id} does not exist"
        )
    return post

@app.post("/posts", status_code=status.HTTP_201_CREATED)
async def create_post(post: schemas.PostBase,db: Session = Depends(get_db)):
    new_post = models.Post(**post.model_dump())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)

    return {"message": "post created", "data": new_post}


@app.post("/posts/valid", status_code=status.HTTP_201_CREATED, response_model=schemas.Post)
async def create_post_valid(post: schemas.PostBase, db: Session = Depends(get_db)):
    new_post = models.Post(**post.model_dump())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)

    return new_post


@app.put("/posts/{id}")
async def update_post(id: int, post_upd: schemas.PostBase, db: Session = Depends(get_db)):
    post = db.query(models.Post).filter(models.Post.id == id)
    if post.first() == None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"post with id: {id} does not exist"
        )
    post.update(post_upd.dict(), synchronize_session=False)
    db.commit()

    return {"data": "post updated"}


@app.delete("/posts/{id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_post(id: int, db: Session = Depends(get_db)):
    post = db.query(models.Post).filter(models.Post.id == id)
    if post.first() == None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"post with id: {id} does not exist"
        )
    post.delete(synchronize_session=False)
    db.commit()

    return {"data": "post deleted"}

@app.get("/author", response_model=list[schemas.Author])
async def all_authors(db: Session = Depends(get_db)):
    authors = db.query(models.Author).all()
    return authors

@app.post("/author", status_code=status.HTTP_201_CREATED)
async def create_author(author: schemas.Author, db: Session = Depends(get_db)):
    # Перед созданием хэшируемые пароль
    # hashed_password = pwd_context.hash(author.password)
    # author.password = hashed_password

    # Использование функции из utils.py
    hashed_password = utils.hash(author.password)
    author.password = hashed_password
    new_author = models.Author(**author.model_dump())
    db.add(new_author)
    db.commit()
    db.refresh(new_author)

    return {"message": "author created", "data": new_author}

@app.get("/posts/verify")
async def all_posts_verify(
        db: Session = Depends(get_db),
        get_current_author: int = Depends(oauth2.get_current_author)):

    posts = db.query(models.Post).all()
    return {"data": posts}

@app.post("/posts/verify", status_code=status.HTTP_201_CREATED)
async def create_post_verify(
        post: schemas.PostBase,
        db: Session = Depends(get_db),
        get_current_author: int = Depends(oauth2.get_current_author)):
    new_post = models.Post(**post.model_dump())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)

    return {"message": "post created", "data": new_post}

@app.get('/author/{id}', response_model=schemas.AuthorOut)
def get_author(id: int, db: Session = Depends(get_db)):
    author = db.query(models.Author).get(id)
    if not author:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Author with id: {id} does not exist"
        )

    return author



@app.put("/connection/{id_post}/{id_author}")
async def connect_post(id_post: int, id_author:int, db: Session = Depends(get_db)):
    post = db.query(models.Post).filter(models.Post.id == id_post).first()
    author = db.query(models.Author).filter(models.Author.id == id_author).first()

    if post == None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"post with id: {id_post} does not exist"
        )

    if author == None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"post with id: {id_author} does not exist"
        )

    post.owner_id = author.id
    db.commit()

    return {"data": "post connected"}

if __name__ == "__main__":
    uvicorn.run(app)