from pydantic import BaseModel, EmailStr
from typing import Optional
from datetime import datetime

class PostBase(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None


class PostCreate(PostBase):
    pass


class UpdatePost(PostBase):
    published: bool


class Post(BaseModel):
    title: str
    content: str
    published: bool

    class Config:
        orm_mode = True

class Author(BaseModel):
    first_name: str
    name: str
    email: EmailStr
    password: str

class AuthorOut(BaseModel):
    id: int
    email: EmailStr


class AuthorLogin(BaseModel):
    email: EmailStr
    password: str


# Делаем формочку для проверки токена
class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    id: Optional[int] = None
